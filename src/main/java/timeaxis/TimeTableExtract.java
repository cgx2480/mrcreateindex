package timeaxis;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@Getter
@Setter
@XmlType
@XmlRootElement(name = "table")
@XmlAccessorType(XmlAccessType.NONE)
public class TimeTableExtract implements Serializable{
	private static final long serialVersionUID = 1L;


	@XmlAttribute(name = "part")
	private String part;				//模型类型：配置对象（time）、配置维度(dimension)、配置表标签（tabletag）
	
	@XmlAttribute(name = "name")
	private String name;				//名称
	
	@XmlAttribute(name = "tablename")
	private String tablename;			//表名
	
	@XmlAttribute(name = "createtime")
	private String createtime;			//创建时间
	
	@XmlAttribute(name = "status")
	private String status;				//是否判断删除，默认值delete
	
	@XmlElement(name = "field")
	private GeneralField timeField;		//主key
	
	@XmlElement(name = "items")
	private TimeItem items;				//内容
	
}
