package timeaxis;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class GeneralDs implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@XmlAttribute(name = "e_field")
	private String field;
	
	@XmlAttribute(name = "c_field")
	private String name;
}
