package timeaxis;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@XmlType
@XmlRootElement(name = "field")
@XmlAccessorType(XmlAccessType.NONE)
public class GeneralField implements Serializable{
	private static final long serialVersionUID = 1L;

	@XmlElement(name = "primarykey")
	private List<GeneralDs> primarykey;
}
