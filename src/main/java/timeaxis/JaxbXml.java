package timeaxis;

import javax.xml.bind.JAXB;
import java.io.StringReader;

public class JaxbXml {

	public static <T> T xmlToObj(String xml, Class<T> clazz) {
		StringReader reader = new StringReader(xml);
		T t = JAXB.unmarshal(reader, clazz);
		return t;
	}
}
