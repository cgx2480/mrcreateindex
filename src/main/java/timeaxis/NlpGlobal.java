package timeaxis;

public class NlpGlobal {

	// xml定义， 不同类型
	/**
	 * 自然人时间维度
	 */
	public static final String NATURE_TIME_MOLD = "1"; 	// 自然人时间维度
	/**
	 * 自然人业务维度[类型]
	 */
	public static final String NATURE_BUS_MOLD  = "2"; 	// 自然人业务维度
	/**
	 * 自然人标签维度[类型]
	 */
	public static final String NATURE_TAG_MOLD  = "3";	// 自然人标签维度
	/**
	 * 法人时间维度[类型]
	 */
	public static final String LEGAL_TIME_MOLD  = "4"; 	// 法人时间维度
	/**
	 * 法人业务维度[类型]
	 */
	public static final String LEGAL_BUS_MOLD 	= "5"; 	// 法人业务维度
	/**
	 * 法人标签维度 [类型]
	 */
	public static final String LEGAL_TAG_MOLD 	= "6";	// 法人标签维度
	

	// xml定义， 执行状态 0:初始 1：更新 2： 完成
	/**
	 * xml定义， 执行状态 [初始]
	 */
	public static final String XML_INIT_STATUS = "0";		//初始
	/**
	 * xml定义， 执行状态 [更新]
	 */
	public static final String XML_UPDATE_STATUS = "1";		//更新
	/**
	 * xml定义， 执行状态 [完成]
	 */
	public static final String XML_COMPLATE_STATUS = "2";	//完成
	
	/**
	 * xml定义删除属性，此属性只判断xml删除且不抽取新数据
	 */
	public static final String XML_STATUS_DELETE = "delete";
}
