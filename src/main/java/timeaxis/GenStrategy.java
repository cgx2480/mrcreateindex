package timeaxis;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;

public class GenStrategy {

	public static GenStrategy builder() {
		return new GenStrategy();
	}
	
	/**
	 * 多属性关联组合，md5 加密压缩 8 位
	 * @param relevance 关联字符
	 * @param args		参数
	 * @return
	 */
	public String getRowkey(String relevance,String... args) {
		String arg = relevanceArg(relevance,args);
		return reduce8Md5Hash(arg);
	}
	
	public String getRowkey(String relevance,String arg_0,long timestamp) {
		StringBuilder rkSb = new StringBuilder();
		String reduce = reduce8Md5Hash(arg_0);
		rkSb.append(reduce).append(relevance).append(timestamp);
		return rkSb.toString();
	}
	
	/**
	 * 关联属性
	 * @param relevance 关联字符
	 * @param args		参数
	 * @return
	 */
	private String relevanceArg(String relevance, String... args) {
		StringBuilder sb = new StringBuilder();
		if(StrUtil.isNotEmpty(relevance) && args != null && args.length > 0) {
			for(String arg : args) {
				sb.append(arg).append(relevance);
			}
			sb.deleteCharAt(sb.length() - relevance.length());
		}
		return sb.toString();
	}
	
	/**
	 * md5 加密压缩 8 位
	 * @param arg0
	 * @return
	 */
	private String reduce8Md5Hash(String arg0) {
		StringBuilder reduce = new StringBuilder();
		String md5Hash = md5Hash(arg0);
		reduce.append(reduce8Str(md5Hash)).append("_").append(arg0);
		return reduce.toString();
	}
	
	private String[] chars = new String[] { "a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z" };
	
	/**
	 * md5 hash加密
	 * @param str
	 * @return
	 */
	private String md5Hash(String str) {
		return DigestUtil.md5Hex(str);
	}
	
	/**
	 * 字符压缩8位
	 * @param origStr
	 * @return
	 */
	private String reduce8Str(String origStr) {
		StringBuilder shortBuffer = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            String str = origStr.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x3E]);
        }
        return shortBuffer.toString();
	}
}
