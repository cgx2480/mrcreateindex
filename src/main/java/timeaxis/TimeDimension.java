package timeaxis;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@Getter
@Setter
@XmlType
@XmlRootElement(name = "model")
@XmlAccessorType(XmlAccessType.NONE)
public class TimeDimension implements Serializable{
	private static final long serialVersionUID = 1L;

	@XmlAttribute(name = "type")
	private String type;				//类型：【lgl:法人;ppl:自然人;】
	
	@XmlAttribute(name = "name")
	private String name;				//名称
	
	@XmlElement(name = "table")
	private TimeTableExtract tableExtract;  //配置抽取对象
}
