package timeaxis;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "items")
@XmlAccessorType(XmlAccessType.NONE)
public class TimeItem implements Serializable{
	private static final long serialVersionUID = 1L;

	@XmlElementWrapper(name="item")
	@XmlElement(name = "eventtime")
	private List<GeneralDs> evtTimes;
}
