package com.outlook.chenxu;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.mapreduce.Job;
import timeaxis.HbaseProps;
import timeaxis.NlpGlobal;

import java.io.IOException;

public class AppMainMR {

    public static void main(String[] args) throws Exception {

        String mode = args[0];
        String sourceTableName = args[1];
        String xmlRowkey = args[2];

        Configuration conf = getConfiguration();
        conf.set("familyName", "info");
        conf.set("mode", mode);

        Table xmlTable = ConnectionFactory.createConnection(conf)
                .getTable(TableName.valueOf("dataMapping:zcx_nature_legal_xml"));
        String xml = Bytes.toString(xmlTable.get(new Get(Bytes.toBytes(xmlRowkey)))
                .getValue(Bytes.toBytes("info"), Bytes.toBytes("xml")));

        conf.set("xml", xml);

        if (xml.length() > 0) {
            System.out.println("\n\n\n获取XML成功\n" + xml + "\n\n");
        }

        Job job = Job.getInstance(conf, "create index for " + conf.get("mode"));
        job.setJarByClass(AppMainMR.class);

        String tableNameSpace = conf.get("name.space");
        StringBuilder targetTableName = getTargetTableName(mode);

        Scan scan = new Scan();
        scan.setCaching(1000);
        scan.setCacheBlocks(false);

        TableMapReduceUtil.initTableMapperJob(
                tableNameSpace + ":" + sourceTableName,
                scan,
                MyMapper.class,
                null,
                null,
                job);

        TableMapReduceUtil.initTableReducerJob(
                targetTableName.toString(),
                null,
                job);

        System.out.println("\n\n源表：" + tableNameSpace + ":" + args[1]);
        System.out.println("\n\n目标表：" + targetTableName + "\n\n");

        job.setNumReduceTasks(0);
        job.submit();

        boolean b = job.waitForCompletion(true);
        if (!b) {
            throw new IOException("error with job!");
        }
    }

    /**
     * 根据业务类型判断目标表
     * @param mode
     * @return
     */
    private static StringBuilder getTargetTableName(String mode) {
        StringBuilder targetTableName = null;
        if (NlpGlobal.NATURE_TIME_MOLD.equals(mode)) {
            // 自然人时间维度
            targetTableName = new StringBuilder("dataMapping:zcx_nature_person_time");
        }
        if (NlpGlobal.NATURE_BUS_MOLD.equals(mode)) {
            // 自然人业务维度
            targetTableName = new StringBuilder("dataMapping:zcx_nature_person_bus");
        }
        if (NlpGlobal.NATURE_TAG_MOLD.equals(mode)) {
            // 自然人标签维度
            targetTableName = new StringBuilder("dataMapping:zcx_nature_person_tag");
        }
        if (NlpGlobal.LEGAL_TIME_MOLD.equals(mode)) {
            // 法人时间维度
            targetTableName = new StringBuilder("dataMapping:zcx_legal_person_time");
        }
        if (NlpGlobal.LEGAL_BUS_MOLD.equals(mode)) {
            // 法人业务维度
            targetTableName = new StringBuilder("dataMapping:zcx_legal_person_bus");
        }
        if (NlpGlobal.LEGAL_TAG_MOLD.equals(mode)) {
            // 法人标签维度
            targetTableName = new StringBuilder("dataMapping:zcx_legal_person_tag");
        }
        return targetTableName;
    }

    private static Configuration getConfiguration() {
        Configuration conf = HBaseConfiguration.create();
        try {
            HbaseProps hbaseProps = HbaseProps.build();
            conf.set("hbase.zookeeper.quorum", hbaseProps.getQuorum());
            conf.set("hbase.zookeeper.property.clientPort", hbaseProps.getClientPort());
            conf.set("hbase.client.scanner.timeout.period", hbaseProps.getPeriod());
            conf.set("hbase.client.keyvalue.maxsize", hbaseProps.getMaxSize());// 列长度无限
            conf.set("dfs.socket.timeout", hbaseProps.getSocketTimeout());
            conf.set("hbase.security.authentication.tbds.secureid", hbaseProps.getHbase_tbds_secureid());
            conf.set("hbase.security.authentication.tbds.securekey", hbaseProps.getHbase_tbds_securekey());
            conf.set("hadoop.security.authentication.tbds.secureid", hbaseProps.getHadoop_tbds_secureid());
            conf.set("hadoop.security.authentication.tbds.securekey", hbaseProps.getHadoop_tbds_securekey());
            conf.set("name.space", hbaseProps.getNamespace());
            for (String url : hbaseProps.getHadoop()) {
                conf.addResource(new Path(url));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conf;
    }
}
