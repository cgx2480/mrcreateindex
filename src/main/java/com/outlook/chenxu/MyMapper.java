package com.outlook.chenxu;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import timeaxis.GenStrategy;
import timeaxis.JaxbXml;
import timeaxis.NlpGlobal;
import timeaxis.TimeDimension;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class MyMapper extends TableMapper<ImmutableBytesWritable, Put> {

    private static String familyName;
    private static String mode;
    private static TimeDimension dimension = new TimeDimension();

    private static LocalDateTime normalTimeFormat(String dateStr) {
        try {
            String date = dateStr.split(" ")[0];
            String time = dateStr.split(" ")[1];
            return LocalDateTime.of(LocalDate.parse(date), LocalTime.parse(time));
        } catch (DateTimeParseException e) {
            return LocalDateTime.ofInstant(Instant.ofEpochSecond(Long.valueOf(dateStr)),
                    ZoneId.systemDefault());
        }

    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        mode = context.getConfiguration().get("mode");
        familyName = context.getConfiguration().get("familyName");
        dimension = JaxbXml.xmlToObj(context.getConfiguration().get("xml"), TimeDimension.class);
        super.setup(context);
    }

    @Override
    protected void map(ImmutableBytesWritable row, Result value, Context context) {
        Objects.requireNonNull(resultToPut(value)).forEach(put -> {
                    try {
                        context.write(row, put);
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
        );
    }

    private List<Put> resultToPut(Result value) {

        if (NlpGlobal.NATURE_TIME_MOLD.equals(mode) || NlpGlobal.LEGAL_TIME_MOLD.equals(mode)) {
            // 自然人时间维度
            return functonForTime(value);
        }
        if (NlpGlobal.NATURE_BUS_MOLD.equals(mode) || NlpGlobal.LEGAL_BUS_MOLD.equals(mode)) {
            // 自然人业务维度
            return functonForBus(value);
        }
        if (NlpGlobal.NATURE_TAG_MOLD.equals(mode) || NlpGlobal.LEGAL_TAG_MOLD.equals(mode)) {
            // 自然人标签维度
            return functonForTag(value);
        }

        return null;
    }

    private List<Put> functonForTag(Result mode) {
        System.out.println("标签维度" + mode);
        return null;
    }

    private List<Put> functonForBus(Result mode) {
        System.out.println("业务为度" + mode);
        return null;
    }

    private List<Put> functonForTime(Result value) {
        System.out.println("时间维度" + mode);

        byte[] table_name = value.getValue(Bytes.toBytes(familyName), Bytes.toBytes("table_name"));
        byte[] name = value.getValue(Bytes.toBytes(familyName), Bytes.toBytes("name"));
        byte[] foreign_rowkey = value.getValue(Bytes.toBytes(familyName), Bytes.toBytes("foreign_rowkey"));

        String event_time_field_name = dimension.getTableExtract().getItems().getEvtTimes().get(0).getField();
        String event_time = Bytes.toString(value.getValue(Bytes.toBytes(familyName), Bytes.toBytes(event_time_field_name)));

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime eventLocalDateTime = normalTimeFormat(event_time);
        String eventTimeFormat = dateTimeFormatter.format(eventLocalDateTime);
        String year = String.valueOf(eventLocalDateTime.getYear());

        String month = String.valueOf(eventLocalDateTime.getMonthValue());
        return dimension.getTableExtract().getTimeField().getPrimarykey()
                .stream()
                .map(pk -> {
                            String primaryKeyInSourceTable = Bytes.toString(value.getValue(Bytes.toBytes(familyName), Bytes.toBytes(pk.getField())));
                            String indexRowkey = GenStrategy.builder().getRowkey("_", primaryKeyInSourceTable, Instant.now().getEpochSecond());

                            Put put = new Put(Bytes.toBytes(indexRowkey));
                            put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("rowkey"), value.getRow());
                            put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("table_name"), table_name);
                            put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("name"), name);
                            put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("foreign_rowkey"), foreign_rowkey);
                            put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("year"), Bytes.toBytes(year));
                            put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("month"), Bytes.toBytes(month));
                            put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("event_time"), Bytes.toBytes(eventTimeFormat));

                            return put;
                        }
                )
                .collect(Collectors.toList());
    }
}
