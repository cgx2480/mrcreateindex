#!/usr/bin/env bash


spark-submit \
--class com.outlook.chenxu.AppMain \
--master yarn \
--deploy-mode cluster \
--executor-memory 1024m \
--num-executors 4 \
--executor-cores 2 \
./SparkReadHbase.jar


spark-submit \
--class com.outlook.chenxu.AppMain \
--master local[*] \
./SparkReadHbase.jar


hadoop jar ./SparkReadHbase.jar \
com.outlook.chenxu.AppMainMR \
ymy_lgl_corp_bsc_info \
zcx_legal_person_time

hadoop jar \
/gd/nlpson-spark/SparkReadHbase.jar \
com.outlook.chenxu.AppMainMR 4 ymy_lgl_corp_bsc_info 0N64QI7L_ymy_lgl_corp_bsc_info_4

java -cp /gd/nlpson-spark/ReadXMLTable.jar com.outlook.chenxu.AppMain
